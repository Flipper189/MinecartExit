package de.flipper189.MinecartExit;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Platform {

	public static void exit(Player p){
		//check if the player is sitting in a minecart
		if ((p.getVehicle() != null) && (p.getVehicle().toString().equals("CraftMinecartRideable"))){
			
			//delete minecart
			p.getVehicle().remove();
			
			//set player one block higher so that he is not stuck in the ground
			Location loc = p.getLocation();
			loc.add(0, 0.3, 0);
			p.teleport(loc);
			
			//if the player is not in creative mode give new minecart
			if(p.getGameMode() != GameMode.CREATIVE){
				if (p.getInventory().firstEmpty() != -1){
					p.getInventory().addItem(new ItemStack(Material.MINECART));
				}
				else{
					loc.getWorld().dropItemNaturally(loc,new ItemStack(Material.MINECART));
				}
			}
			
			//teleport player to platform if available
			teleportIfPossible(p);
		}
	}
	
	
	public static void teleportIfPossible(Player p){
		
		final  MinecartExit plugin = MinecartExit.getPlugin(); 
		
		//check if the configuration is complete
		if (plugin.getConfig().contains("platform.exit.enable") && plugin.getConfig().contains("platform.exit.material") && plugin.getConfig().contains("platform.exit.height")){

			String configEnable = plugin.getConfig().getString("platform.exit.enable");
			String configHeight = plugin.getConfig().getString("platform.exit.height");
			String configMaterial = plugin.getConfig().getString("platform.exit.material");
			
			double platformHeight = 0;
			try{
				platformHeight = Double.parseDouble(configHeight);
			}
			catch(Exception e){	
			}
			
			if (configEnable == "true"){				
				if (platformHeight == 0.5 || platformHeight == 1.0){
					if(Material.getMaterial(configMaterial) != null){
						Location loc = p.getLocation();
						
						//convert player position to block position
						Location blocklocW = loc.getBlock().getLocation(); 
						Location blocklocO = loc.getBlock().getLocation(); 
						Location blocklocS = loc.getBlock().getLocation(); 
						Location blocklocN = loc.getBlock().getLocation(); 
						
						//prepare block position for environment query
						blocklocW.add(-1, 0, 0);
						blocklocO.add(1, 0, 0);
						blocklocS.add(0, 0, 1);
						blocklocN.add(0, 0, -1);
						
						Material platformMaterial = Material.getMaterial(configMaterial);
						
						//check surroundings for platform
						//west
						if (blocklocW.getBlock().getType() == platformMaterial && ExitPosition.isFree(blocklocW)){
							p.teleport(loc.add(-1, platformHeight, 0));	
						}
						//east
						else if (blocklocO.getBlock().getType() == platformMaterial && ExitPosition.isFree(blocklocO)){
							p.teleport(loc.add(1, platformHeight, 0));		
						}
						//south
						else if (blocklocS.getBlock().getType() == platformMaterial && ExitPosition.isFree(blocklocS)){
							p.teleport(loc.add(0, platformHeight, 1));	
						}
						//north
						else if (blocklocN.getBlock().getType() == platformMaterial && ExitPosition.isFree(blocklocN)){
							p.teleport(loc.add(0, platformHeight, -1));
						}
					
					}
					else{
						plugin.getLogger().info(plugin.getConfig().getString("platform.exit.material") + " is not a valid material!");
					}
				}	
				
				else{
					plugin.getLogger().info(plugin.getConfig().getString("platform.exit.height")+" is not a valid platform height!");
					plugin.getLogger().info("platform.exit.height may only have the value of 0.5 or 1!");
				}
	
			}	
			
		}
		else{
			plugin.getLogger().info("Configuration file is incomplete!");
		}
		
	}
}