package de.flipper189.MinecartExit;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;


public class MinecartExit extends JavaPlugin  implements Listener{

	private static MinecartExit plugin;
	public static MinecartExit getPlugin(){
		return plugin;
	}
	

	@Override
	public void onEnable() {
		MinecartExit.plugin = this;
		
		//prepare default configuration
		getConfig().addDefault("platform.exit.enable", true);
		getConfig().addDefault("platform.exit.material", "STONE_SLAB");
		getConfig().addDefault("platform.exit.height", 0.5);
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		//register event
		this.getServer().getPluginManager().registerEvents(this, this);		
	}
	

	@Override
	public void onDisable() {
	}
	
	//ClickEvent
	@EventHandler
	public void onMinecartClick(PlayerInteractEvent clickEvent){
		try{
			Block b = clickEvent.getClickedBlock();
			Player p = clickEvent.getPlayer();
			if (b.getType() == Material.RAIL || b.getType() == Material.DETECTOR_RAIL || b.getType() == Material.ACTIVATOR_RAIL || b.getType() == Material.POWERED_RAIL)
			{
				//FunktionAussteigen.exit(p);
				Platform.exit(p);
				
			}
		}
		catch(java.lang.NullPointerException e){
		}
	}
	
}