package de.flipper189.MinecartExit;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class ExitPosition {
	
	//check on the foot and head position is not placed with blocks
	public static boolean isFree (Location BlockLoc){
		Location blockLocationFoot = BlockLoc.getBlock().getLocation();
		Location blockLocationHead = BlockLoc.getBlock().getLocation();

		blockLocationFoot.add(0, 1, 0);
		blockLocationHead.add(0, 2, 0);
		
		return blockIsNotSolid(blockLocationFoot) && blockIsNotSolid(blockLocationHead);
	}
	
	public static boolean blockIsNotSolid (Location BlockLoc){
		Block b = BlockLoc.getBlock();
		if (b.getType() == Material.AIR 
			|| b.getType() == Material.TORCH
			|| b.getType() == Material.WALL_TORCH
			|| b.getType() == Material.REDSTONE_TORCH
			|| b.getType() == Material.REDSTONE_WALL_TORCH
			|| b.getType() == Material.LEVER
			|| b.getType() == Material.STONE_BUTTON
			|| b.getType() == Material.ACACIA_BUTTON
			|| b.getType() == Material.BIRCH_BUTTON
			|| b.getType() == Material.DARK_OAK_BUTTON
			|| b.getType() == Material.JUNGLE_BUTTON
			|| b.getType() == Material.OAK_BUTTON
			|| b.getType() == Material.SPRUCE_BUTTON
			|| b.getType() == Material.LADDER
			|| b.getType() == Material.VINE
			|| b.getType() == Material.SNOW
			|| b.getType() == Material.SIGN
			|| b.getType() == Material.WALL_SIGN
			){
				return true;
			}
		return false;
	}

}