# MinecartExit


## Description
MinecartExit is a plugin for Minecraft Spigot and Paper Server. With MinecartExit you can exit a minecart while riding without the minecrat driving away.


## Usage
- click on the bottom of the minecart during the ride to get off, the minecart will then be given to you in inventory
- in the configuration you can set a block for a platform on which you will be teleported when you get off the minecart if there is a platform.


## Installation
Put the .jar file in the plugins folder from your server and restart the server.


## Requirements
Spigot/Paper Server 1.13.2 or higher


## Download
[Version 1.0.4](https://gitlab.com/Flipper189/MinecartExit/-/blob/main/build/MinecartExit-1.0.4.jar)


[Change Log](https://gitlab.com/Flipper189/MinecartExit/-/blob/main/CHANGELOG)


## License
[GNU GPLv3](https://gitlab.com/Flipper189/MinecartExit/-/blob/main/LICENSE)